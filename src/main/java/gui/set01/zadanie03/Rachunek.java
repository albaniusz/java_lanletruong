package gui.set01.zadanie03;

/**
 * @author s8376
 */
public class Rachunek {

	protected double _balance;
	protected static int _counter;
	protected int _number;
	protected Osoba _owner;

	/**
	 * @param owner
	 */
	public Rachunek(Osoba owner) {

		_owner = owner;
		_counter++;
		_number = _counter;
	}

	/**
	 * @param ammount
	 * @return
	 */
	protected boolean _checkBalanceWithdraw(int ammount) {
		return (_balance - ammount < 0 ? false : true);
	}

	/**
	 *
	 */
	public void aktualizuj() {
	}

	/**
	 * @return
	 */
	public double getBalance() {
		return _balance;
	}

	/**
	 * @return
	 */
	public int getNumber() {
		return _number;
	}

	/**
	 * @param target
	 * @param ammount
	 */
	public void przelew(Rachunek target, int ammount) {

		if (_checkBalanceWithdraw(ammount)) {
			_balance -= ammount;
			target.wplata(ammount);
		} else {
			System.out.println(this + " - nie dozwolona operacja: przelew!");
		}
	}

	/**
	 * @return
	 */
	@Override
	public String toString() {
		return "Konto nr " + _number + ": " + _owner + ", stan " + _balance;
	}

	/**
	 * @param ammount
	 */
	public void wplata(int ammount) {
		_balance += ammount;
	}

	/**
	 * @param ammount
	 */
	public void wyplata(int ammount) {

		if (_checkBalanceWithdraw(ammount)) {
			_balance -= ammount;
		} else {
			System.out.println(this + " - nie dozwolona operacja: wypłata!");
		}
	}
}
