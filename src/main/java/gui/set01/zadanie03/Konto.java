package gui.set01.zadanie03;

/**
 * Rachunek
 *
 * @author s8376
 */
public class Konto extends Rachunek {

	protected double _interest;

	/**
	 * @param owner
	 * @param interest
	 */
	public Konto(Osoba owner, int interest) {
		super(owner);
		_interest = (double) interest;
	}

	/**
	 *
	 */
	@Override
	public void aktualizuj() {
		_balance += _balance * ((double) _interest / 100);
	}

	/**
	 * @return
	 */
	@Override
	public String toString() {
		return super.toString() + ", oprocentowanie " + _interest;
	}
}
