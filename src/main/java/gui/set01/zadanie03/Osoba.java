package gui.set01.zadanie03;

/**
 * Osoba
 *
 * @author s8376
 */
public class Osoba {

	protected String name;

	/**
	 * @param name
	 */
	public Osoba(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	@Override
	public String toString() {
		return name;
	}
}
