package gui.set01.zadanie02;

/**
 * Punkt
 *
 * @author s8376
 */
public class Punkt {

	protected int x;
	protected int y;

	/**
	 * @param x
	 * @param y
	 */
	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return
	 */
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
