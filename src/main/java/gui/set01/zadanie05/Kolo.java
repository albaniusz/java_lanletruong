package gui.set01.zadanie05;

/**
 * Kolo
 */
public class Kolo extends Figura implements Obliczenie {

	private int promien;
	/**
	 * Nazwa figury
	 */
	protected String fig = "Kolo";

	/**
	 * konstruowanie koła o środku w punkcie (x, y) i  promieniu r
	 *
	 * @param x
	 * @param y
	 * @param r
	 */
	public Kolo(int x, int y, int r) {
		super(x, y);
		promien = r;
	}

	/**
	 *
	 */
	public void obwod() {

	}

	/**
	 *
	 */
	public void pole() {

	}

	/**
	 * @param x
	 * @param y
	 */
	public void pozycja(int x, int y) {

		String message = "Punkt (" + x + ", " + y + ") znajduje sie ";

		if (x > this.x + promien || y > this.y + promien) {
			message += "na zewnatrz kola";
		} else {
			message += "wewnatrz kola";
		}

		System.out.println(message);
	}

	/**
	 * wypisuje dodatkowo promień koła
	 */
	public void pokaz() {
		super.pokaz();
		System.out.println("Promien - " + promien);
	}
}
