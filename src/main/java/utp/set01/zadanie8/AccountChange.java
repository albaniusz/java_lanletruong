package utp.set01.zadanie8;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * AccountChange
 */
public class AccountChange implements PropertyChangeListener {

	/**
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("Zmiana stanu konta z " + evt.getOldValue()
				+ " na: " + evt.getNewValue());
	}
}
