package utp.set01.zadanie8;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

/**
 * AccountLimitator
 */
public class AccountLimitator implements VetoableChangeListener {

	/**
	 * @param evt
	 * @throws PropertyVetoException
	 */
	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {

		Account a = (Account) evt.getSource();
		if ((Double) evt.getNewValue() < ((Double) evt.getOldValue() - a.getDebitLimit())) {
			throw new PropertyVetoException("Przekroczenie limitu konta", evt);
		}
	}
}
