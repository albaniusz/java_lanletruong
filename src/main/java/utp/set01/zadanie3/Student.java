package utp.set01.zadanie3;

/**
 * Student
 */
public class Student implements Comparable<Student> {

	private String name;
	private String surname;
	private Integer index;
	private Float average;

	/**
	 * @param params
	 */
	public Student(String params) {

		try {
			String input[] = params.split("([ ]+)");
			setName(input[0]);
			setSurname(input[1]);
			setIndex(Integer.parseInt(input[2]));
			setAverage(Float.parseFloat(input[3]));
		} catch (Exception e) {
			System.out.println("Niepoprawne dane, pomijam");
		}
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @return
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @return
	 */
	public Float getAverage() {
		return average;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @param index
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * @param average
	 */
	public void setAverage(Float average) {
		this.average = average;
	}

	/**
	 * @return
	 */
	public String toString() {
		return getName() + " " + getSurname() + " " + getIndex() + " "
				+ getAverage();
	}

	/**
	 * @param student
	 * @return
	 */
	public int compareTo(Student student) {

		int comparePoints = 0;

		if (getIndex() < student.getIndex()) {
			comparePoints += 100;
		}

		comparePoints += getSurname().compareTo(student.getSurname());
		comparePoints += getName().compareTo(student.getName());

		if (getAverage() < student.getAverage()) {
			comparePoints++;
		}

		return comparePoints;
	}
}
