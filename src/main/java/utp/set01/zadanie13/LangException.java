package utp.set01.zadanie13;

/**
 * LangException
 */
public interface LangException {
	public String[] getNames();
}
