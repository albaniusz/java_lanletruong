package utp.set01.zadanie13;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Location
 */
public class Location extends JPanel {

	private final DateFormat _dateFormatDate;
	private final DateFormat _dateFormatTime;
	private final JLabel _date;
	private final JLabel _name;
	private final JLabel _time;

	/**
	 * @param locationName
	 * @param language
	 * @param country
	 * @param timeZoneName
	 * @param langException
	 */
	public Location(String locationName, String language, String country,
	                String timeZoneName, LangException langException) {

		Locale locale = new Locale(language, country);
		TimeZone timeZone = TimeZone.getTimeZone(timeZoneName);

		if (langException != null) {
			DateFormatSymbols dfs = new DateFormatSymbols();
			dfs.setMonths(langException.getNames());
			_dateFormatDate = new SimpleDateFormat("dd MMMM yyyy", dfs);
		} else {
			_dateFormatDate = DateFormat.getDateInstance(DateFormat.LONG,
					locale);
		}
		_dateFormatDate.setTimeZone(timeZone);

		_dateFormatTime = DateFormat
				.getTimeInstance(DateFormat.DEFAULT, locale);
		_dateFormatTime.setTimeZone(timeZone);

		//        setPreferredSize(new Dimension(200, 200));
		setLayout(new GridLayout(3, 1));

		_name = new JLabel(locationName);
		add(_name);

		_date = new JLabel(getDate());
		add(_date);

		_time = new JLabel(getTime());
		add(_time);
	}

	/**
	 * @param locationName
	 * @param language
	 * @param country
	 * @param timeZoneName
	 */
	public Location(String locationName, String language, String country,
	                String timeZoneName) {
		this(locationName, language, country, timeZoneName, null);
	}

	public String getDate() {
		return _dateFormatDate.format(new Date());
	}

	public String getTime() {
		return _dateFormatTime.format(new Date());
	}

	/**
	 * @param g
	 */
	@Override
	public void paint(Graphics g) {

		super.paint(g);

		_date.setText(getDate());
		_time.setText(getTime());
	}
}
