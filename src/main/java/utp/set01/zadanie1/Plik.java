package utp.set01.zadanie1;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Plik
 */
public class Plik extends File {

	/**
	 * @param path
	 */
	public Plik(String path) {
		super(path);
	}

	/**
	 * @param list
	 * @param file
	 * @param filter
	 */
	private void _parse(List<File> list, File file, FilenameFilter filter) {

		Plik tmp;

		for (String s : file.list(filter)) {
			tmp = new Plik(file.getPath() + "/" + s);

			if (tmp.isFile()) {
				list.add(tmp);
			} else if (tmp.isDirectory()) {
				_parse(list, tmp, filter);
			}
		}
	}

	/**
	 * @return
	 */
	public List<File> listFilesRecurse() {

		List<File> list = new ArrayList<File>();
		_parse(list, this, null);

		return list;
	}

	/**
	 * @param filter
	 * @return
	 */
	public List<File> listFilesRecurse(FilenameFilter filter) {

		List<File> list = new ArrayList<File>();
		_parse(list, this, filter);

		return null;
	}

	/**
	 * @return
	 */
	public String getTree() {

		String fullPath = getPath();

		fullPath += getPath();

		return fullPath;
	}
}
