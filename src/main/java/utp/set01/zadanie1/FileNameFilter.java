package utp.set01.zadanie1;

import java.io.File;
import java.io.FilenameFilter;

/**
 * FileNameFilter
 */
public class FileNameFilter implements FilenameFilter {

	/**
	 * @param dir
	 * @param name
	 * @return
	 */
	public boolean accept(File dir, String name) {
		return (name.endsWith(".class"));
	}
}
