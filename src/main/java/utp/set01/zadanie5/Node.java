package utp.set01.zadanie5;

/**
 * Node<T>
 *
 * @param <T>
 */
public class Node<T> implements Comparable<T> {

	T value;
	Node<T> next, prev;

	/**
	 * Nowy węzeł z podanymi składowymi,
	 *
	 * @param value
	 * @param next
	 * @param prev
	 */
	public Node(T value, Node<T> next, Node<T> prev) {
		this.value = value;
		this.next = next;
		this.prev = prev;
	}

	/**
	 * Wypisuje informacje o wezle
	 *
	 * @return
	 */
	public String toString() {
		return "" + value;
	}

	/**
	 * @return
	 */
	public Node<T> getNext() {
		return next;
	}

	public Node<T> getPrev() {
		return prev;
	}

	public T getValue() {
		return value;
	}

	public void setNext(Node<T> t) {
		next = t;
	}

	public void setPrev(Node<T> t) {
		prev = t;
	}

	/**
	 * @param o
	 * @return
	 */
	public int compareTo(T o) {
//        if (value < o) {
//            
//        }

		return 0;
	}
}
