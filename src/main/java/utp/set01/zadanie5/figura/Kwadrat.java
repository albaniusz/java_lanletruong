package utp.set01.zadanie5.figura;

/**
 * Kwadrat
 */
public class Kwadrat extends Figura {

	private final int a;

	/**
	 * @param a
	 */
	public Kwadrat(int a) {

		super();

		this.a = a;
		o = 4 * a;
		p = a * a;
	}
}
