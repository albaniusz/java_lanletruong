package utp.set01.zadanie5.figura;

/**
 * Figura
 */
public abstract class Figura implements Comparable<Figura> {

	public static int nubmerCounter = 0;
	protected int number;
	protected int o;
	protected int p;

	/**
	 *
	 */
	public Figura() {
		nubmerCounter++;
		number = nubmerCounter;
	}

	/**
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(Figura o) {
		return 0;
	}

	public static int getNubmerCounter() {
		return nubmerCounter;
	}

	public static void setNubmerCounter(int nubmerCounter) {
		Figura.nubmerCounter = nubmerCounter;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getO() {
		return o;
	}

	public void setO(int o) {
		this.o = o;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}
}
