package ppj.set02;

import javax.swing.*;

/**
 * Zadanie 37
 *
 * Napisać i przetestować w programie metodę boolean digits(String s) sprawdzającą,
 * czy w danym łańcuchu tekstowym s występują wyłącznie cyfry.
 *
 * @author s8376
 */
public class Zadanie37 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (digits(JOptionPane.showInputDialog("Podaj ciąg znaków:"))) {
			JOptionPane.showMessageDialog(null,
					"Ciąg znaków zawiera tylko cyfry");
		} else {
			JOptionPane.showMessageDialog(null,
					"Ciąg znaków zawiera inne znaki oprócz cyfr");
		}
	}

	/**
	 * @param s
	 * @return
	 */
	static public boolean digits(String s) {

		if (s.matches("(^[0-9]+)")) {
			return true;
		} else {
			return false;
		}
	}
}
