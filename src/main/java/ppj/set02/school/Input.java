package ppj.set02.school;

import javax.swing.*;

/**
 * Input
 *
 * @author s8376
 * @package school
 */
public class Input {

	/**
	 * Zwraca liczbe int wprowadzona przez uzytkownika
	 *
	 * @param message
	 * @return
	 */
	public int getInt(String message) {

		if (message == "") {
			message = "Podaj liczbę typu int";
		}
		boolean flag = false;
		int value = 0;

		do {
			flag = false;
			String data = JOptionPane.showInputDialog(message);
			try {
				value = Integer.parseInt(data);
				flag = true;
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,
						"Podana wartość nie jest liczbą!");
			}
		} while (flag == false);

		return value;
	}
}
