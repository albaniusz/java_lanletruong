package ppj.set02.school;

/**
 * publiczna klasa testujaca
 */
public class Obiekty {

	/**
	 * funkcja glowna
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		Osoba o1 = new Osoba("Kowalska", 20);
		Osoba o2 = new Osoba();

		System.out.println(o1.wiek); // 20

		// System.out.println(o2.nazwisko); // B³¹d
		System.out.println(o2.dajNazwisko()); // Nowak

		o2.ustawWiek(30);

		o2.pokaz(); // "Osoba (Nowak,30)"
		System.out.println(o2); // "Osoba (Nowak,30)"

		System.out.println("Ró¿nica wieku " + o1.dajNazwisko() + " i "
				+ o2.dajNazwisko() + " to: " + o2.roznicaWieku(o1)); // Ró¿nica
		// wieku
		// Kowalska
		// i
		// Nowak
		// to:
		// 10

		o1.mlodsza(o2).pokaz(); // Osoba (Kowalska,20)
	}

} // koniec klasy Obiekty

/**
 * klasa zewnetrzna Osoba
 */
class Osoba {

	/**
	 * deklaracje pol (zmiennych) obiektowych
	 */
	private String nazwisko;
	int wiek;

	/**
	 * konstruktor 1
	 *
	 * @param nazw
	 * @param w
	 */
	public Osoba(String nazw, int w) {
		nazwisko = nazw; // this.nazwisko
		wiek = w; // this.wiek
	}

	/**
	 * konstruktor 2
	 */
	public Osoba() {
		nazwisko = "Nowak"; // this.nazwisko
		wiek = 1; // this.wiek
		// this("Nowak", 1);
	}

	/**
	 * metoda zwracajaca nazwisko danej osoby
	 *
	 * @return
	 */
	public String dajNazwisko() {
		return nazwisko; // this.nazwisko
	}

	/**
	 * metoda zwracajaca wiek danej osoby
	 *
	 * @return
	 */
	public int dajWiek() {
		return wiek; // this.wiek
	}

	/**
	 * metoda ustalajaca nazwisko
	 *
	 * @param nazw
	 */
	public void ustawNazwisko(String nazw) {
		nazwisko = nazw; // this.nazwisko
	}

	/**
	 * metoda ustalajaca wiek
	 *
	 * @param w
	 */
	public void ustawWiek(int w) {
		wiek = w; // this.wiek
	}

	/**
	 * metoda zwracajaca roznice wieku 2 osob
	 *
	 * @param o
	 * @return
	 */
	public int roznicaWieku(Osoba o) {
		int roznica = wiek - o.wiek; // this.wiek - o.wiek
		return roznica;
	}

	/**
	 * metoda zwracajaca mlodsza osobe z 2 osob
	 *
	 * @param os
	 * @return
	 */
	public Osoba mlodsza(Osoba os) {

		if (roznicaWieku(os) < 0) {
			return this;
		} else {
			return os;
		}
	}

	/**
	 * metoda pokazujaca informacje o danej osobie
	 */
	public void pokaz() {
		System.out.println("Osoba (" + nazwisko + "," + wiek + ")");
	}

	/**
	 * reprezentacja tekstowa
	 *
	 * @return
	 */
	public String toString() {
		return ("Osoba (" + nazwisko + "," + wiek + ")");
	}

} // koniec klasy Osoba
