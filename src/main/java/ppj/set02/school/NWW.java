package ppj.set02.school;

/**
 * NWW - Najmniejsza wspolna wielokrotnosc
 *
 * @author s8376
 */
public class NWW {

	/**
	 * NWD Najwiekszy wspolny dzielnik
	 */
	protected NWD _nwd;

	/**
	 * Konstruktor
	 */
	public NWW() {
		_nwd = new NWD();
	}

	/**
	 * Oblicza najmniejsza wspolna wielokrotnosc dla liczb a i b
	 *
	 * @param a
	 * @param b
	 * @return
	 */
	public int calculate(int a, int b) {
		return (a * b) / _nwd.calculate(a, b);
	}
}
