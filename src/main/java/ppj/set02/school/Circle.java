package ppj.set02.school;

/**
 * Circle
 *
 * @author s8376
 */
public class Circle {

	/**
	 * Punkt srodka okregu
	 */
	protected Point _center;
	/**
	 * Ilosc tworzonych okregow do chwili obecnej
	 */
	protected static int _ilosc;
	/**
	 * Numer danego kola
	 */
	protected int _nr;
	/**
	 * Promien okregu
	 */
	protected int _radius;

	/**
	 * Domyslny konstruktor
	 */
	public Circle() {
		_nr = ++_ilosc;
		_center = new Point();
		_radius = 1;
	}

	/**
	 * @param r
	 */
	public Circle(int r) {
		_nr = ++_ilosc;
		_center = new Point();
		_radius = r;
	}

	/**
	 * @param p
	 * @param r
	 */
	public Circle(Point p, int r) {
		_nr = ++_ilosc;
		_center = p;
		_radius = r;
	}

	/**
	 * Zwraca pole okregu
	 *
	 * @return
	 */
	public double area() {
		double pi = 3.1415926535;
		return pi * (_radius * _radius);
	}

	/**
	 * Zwraca liczbe tworzonych kol do chwili obecnej
	 *
	 * @return
	 */
	public static int dajIlosc() {
		return _ilosc;
	}

	/**
	 * Zwraca numer danego kola
	 *
	 * @return
	 */
	public int dajNr() {
		return _nr;
	}

	/**
	 * Sprawdza, czy punkt jest wewnatrz kola
	 *
	 * @param p
	 * @return
	 */
	public boolean inside(Point p) {

		if (getRadius() >= p.distance(getCenter())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sprawdza, czy nasz okrag jest wiekszy niz okrag c
	 *
	 * @param c
	 * @return
	 */
	public boolean isBigger(Circle c) {
		if (this.area() > c.area()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Zwraca srodek okregu
	 *
	 * @return
	 */
	public Point getCenter() {
		return _center;
	}

	/**
	 * Zwraca promien okregu
	 *
	 * @return
	 */
	public int getRadius() {
		return _radius;
	}

	/**
	 * Ustawia srodek okregu
	 *
	 * @param c
	 */
	public void setCenter(Point c) {
		_center = c;
	}

	/**
	 * Ustawia promien okregu
	 *
	 * @param r
	 */
	public void setRadius(int r) {
		_radius = r;
	}

	/**
	 * Wyswietla informacje o okregu
	 */
	public void show() {
		System.out.println(this.toString());
	}

	/**
	 * @return
	 */
	public String toString() {
		return ("Okrąg o środku w punkcie x: " + _center.getX() + " y: "
				+ _center.getY() + " i promieniu r: " + _radius);
	}
}
