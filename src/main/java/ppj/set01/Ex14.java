package ppj.set01;

import java.util.Scanner;

/**
 * Ex14
 *
 * Napisać program, który rozwiązuje równanie: a*x*x + b*x + c = 0.
 * Współczynniki rzeczywiste podać w inicjacji odpowiednich zmiennych
 * (typu double) w programie.
 */
public class Ex14 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("a*x*x + b*x + c = 0");

		Scanner in = new Scanner(System.in);

		double a, b, c, x1, x2, d;
		a = Double.parseDouble(in.nextLine());
		b = Double.parseDouble(in.nextLine());
		c = Double.parseDouble(in.nextLine());

		System.out.println("Równanie: " + a + "x^2 + " + b + "x + " + c + " = 0");

		d = b * b - 4.0 * a * c;

		System.out.println("Delta: " + d);

		if (d < 0.0) {
			System.out.println("Równanie nie ma rozwiązań w przestrzeni liczb rzeczywistych");
		} else {
			x1 = (b * -1.0) - Math.sqrt(b * b - 4.0 * a * c);
			x2 = (b * -1.0) + Math.sqrt(b * b - 4.0 * a * c);

			if (d == 0.0) {
				System.out.println("Równanie ma jedno rozwiązanie: " + x1);
			} else {
				System.out.println("Równanie ma dwa rozwiązania: " + x1 + ", " + x2);
			}
		}
	}
}
