package ppj.set01;

/**
 * Ex11
 *
 * Napisać program przekształcający dane o temperaturze podane w skali
 * Fahrenheita do skali Celsjusza. Dane wejściowe (w skali F.) podać w inicjacji
 * odpowiedniej zmiennej (typu double) w programie. Konwertować wynik
 * do typu int.
 */
public class Ex11 {

	public static void main(String[] args) {

		double temp = 23;

		System.out.println("Temperatura w C: " + temp);
		System.out.println("Temperatura w F: " + (int) (temp * (9.0 / 5.0) + 32.0));
	}
}
