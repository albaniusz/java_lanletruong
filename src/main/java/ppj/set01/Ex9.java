package ppj.set01;

/**
 * Ex9
 *
 * Napisać program, który za pomocą jednego wywołania System.out.println()
 * wypisze na konsoli następujące wyrażenie matematyczne, gdzie x oznacza wynik
 * wyrażenia obliczony przez program.
 */
public class Ex9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("1+2*3+4\n" +
				"---------------+78\n" +
				"5-6\n" +
				"----------------------=x\n" +
				"10\n" +
				"9- ----\n" +
				"11");
	}
}
