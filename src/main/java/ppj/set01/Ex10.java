package ppj.set01;

/**
 * Ex10
 *
 * Napisać program, który dla 2 liczb naturalnych podanych w programie,
 * wyprowadza ich sumę, różnicę, iloczyn, iloraz, resztę z dzielenia, wynik
 * dzielenia całkowitego z opisem
 */
public class Ex10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int x = 323;
		int y = 21;

		System.out.println("x = " + x);
		System.out.println("y = " + y);
		System.out.println("x+y = " + (x + y));
		System.out.println("x-y = " + (x - y));
		System.out.println("x*y = " + (x * y));
		System.out.println("x:y = " + (x / y));
		System.out.println("x%y = " + (x % y));
		System.out.println("x/y = " + (x / y));
	}
}
