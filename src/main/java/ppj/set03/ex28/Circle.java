package ppj.set03.ex28;

import ppj.set03.ex27.Point;

import static java.lang.Math.pow;

/**
 * Circle
 */
public class Circle {

	private Point center;
	private int radius;

	/**
	 * tworzy koło z środkiem (0,0) i promieniem 1
	 */
	public Circle() {
		this(1);
	}

	/**
	 * tworzy koło z środkiem w punkcie (0,0), i promieniem całkowitym r
	 *
	 * @param r
	 */
	public Circle(int r) {
		this(new Point(), r);
	}

	/**
	 * tworzy koło z środkiem w punkcie p, i promieniem całkowitym r
	 *
	 * @param p
	 * @param r
	 */
	public Circle(Point p, int r) {
		center = p;
		radius = r;
	}

	/**
	 * zwraca środek koła typu Point
	 *
	 * @return
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * zwraca promień koła typu int
	 *
	 * @return
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * ustawia środek koła typu Point
	 *
	 * @param c
	 */
	public void setCenter(Point c) {
		center = c;
	}

	/**
	 * ustawia promień koła typu int
	 *
	 * @param r
	 */
	public void setRadius(int r) {
		radius = r;
	}

	/**
	 * wyprowadza na konsolę informację o kole
	 */
	public void show() {
		System.out.println(toString());
	}

	/**
	 * sprawdza, czy punkt p leży wewnątrz danego koła
	 *
	 * @param p
	 * @return
	 */
	public boolean inside(Point p) {
		return center.distance(p) < radius ? true : false;
	}

	/**
	 * sprawdza, czy dane koło jest większe niż koło c
	 *
	 * @param c
	 * @return
	 */
	public boolean isBigger(Circle c) {
		return getRadius() > c.getRadius() ? true : false;
	}

	/**
	 * zwraca pole koła typu double.
	 *
	 * @return
	 */
	public double area() {
		return Math.PI * pow(radius, 2);
	}

	/**
	 * zwraca true jeśli dane koło i koło p się przecinają, false w przeciwnym przypadku.
	 *
	 * @param p
	 * @return
	 */
	public boolean intersection(Circle p) {
		return center.distance(p.getCenter()) < radius + p.getRadius() ? true : false;
	}

	/**
	 * @return
	 */
	@Override
	public String toString() {
		return "Okrąg: w punkcie " + center.getX() + "," + center.getY()
				+ " i promieniu " + radius;
	}
}
