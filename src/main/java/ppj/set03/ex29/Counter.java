package ppj.set03.ex29;

/**
 * Counter
 */
public class Counter {

	private final static int INITIAL_VALUE = 0;

	/**
	 * stan licznika
	 */
	private int state;
	/**
	 * rejestruje ilość operacji wyk. na obiektach klasy Counter do chwili obecnej
	 */
	private static int op = 0;

	/**
	 * ustawia początkowy stan = 0
	 */
	public Counter() {
		state = INITIAL_VALUE;
	}

	/**
	 * zwraca aktualny stan licznika
	 *
	 * @return
	 */
	public int state() {
		return op;
	}

	/**
	 * zwiększa stan o 1 i zwraca  licznik
	 *
	 * @return
	 */
	public Counter inc() {

		op++;
		state++;

		return this;
	}

	/**
	 * zmniejsza stan o 1 i zwraca licznik
	 *
	 * @return
	 */
	public Counter dec() {

		op++;
		state--;

		return this;
	}

	/**
	 * zerować licznik
	 */
	public void reset() {
		op++;
		state = INITIAL_VALUE;
	}

	/**
	 * zwraca liczbę operacji wyk. na obiektach klasy Counter do chwili obecnej.
	 *
	 * @return
	 */
	public static int getOp() {
		return op;
	}
}
