package ppj.set03;

/**
 * Zadanie 21
 *
 * Napisać program, który wyprowadza na konsolę wszystkie liczby parzyste,
 * których suma nie przekracza podanej liczby. Dane wejściowe podać w oknie
 * dialogowym.
 */
public class Ex21 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int limit = 100;
		int number = 0;
		int sum = 0;

		while (true) {
			number += 2;
			sum += number;

			if (sum >= limit) {
				break;
			}

			System.out.println(number + " (" + sum + ")");
		}
	}
}
