package ppj.set03;

import ppj.set03.ex30.Complex;

/**
 * ZADANIE 30
 *
 * Zaprojektować i testować klasę Complex reprezentującą liczby zespolone:
 * Pola:                    private double  im, re - część rzeczywista
 * i zesplona
 * Konstruktory:     public Complex() - konstruuje liczbę zespoloną: 0 + i*0
 * public  Complex(double re, double im) - konstruuje liczbę zespoloną: re + i*im
 * public  Complex(double re) - konstruuje liczbę zespoloną: re + i*0
 * public Complex(Complex c) - konstruktor "kopiujący", tworzący identyczną
 * kopię liczby zespolonej c
 * Metody:          public double getRe() - zwraca re
 * public double getIm() - zwraca im
 * public String toString() - zwraca tekstową reprezentację liczby zespolonej,
 * w postaci: re + i*im
 * public Complex add(Complex c) - zwraca nową liczbę zespoloną będącą sumą
 * dwóch liczb zespolonych
 * public Complex mult(double a) - zwraca nową liczbę zespoloną poprzez mnożenie
 * przez liczbę rzeczywistą a
 * public Complex mult(Complex c) - zwraca nową liczbę zespoloną poprzez
 * mnożenie przez liczbę zespoloną c
 */
public class Ex30 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Complex complex1 = new Complex();
		Complex complex2 = new Complex(1);
		Complex complex3 = new Complex(1, 1);
		Complex complex4 = new Complex(complex3);

		System.out.println(complex1);
		System.out.println(complex2);
		System.out.println(complex3);
		System.out.println(complex4);

		Complex complex5 = complex1.add(complex3);
		System.out.println("add: " + complex5);

		Complex complex6 = new Complex(2, 2);
		Complex complex7 = complex4.mult(complex6);
		System.out.println("mul: " + complex7);

		Complex complex8 = complex6.mult(2);
		System.out.println("mul: " + complex8);
	}
}
