package ppj.set03.ex27;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Point
 */
public class Point {

	private int x;
	private int y;

	/**
	 * tworzy punkt (0,0)
	 */
	public Point() {
		this(0, 0);
	}

	/**
	 * tworzy punkt (a,0), gdzie a jest liczbą całkowitą
	 *
	 * @param a
	 */
	public Point(int a) {
		this(a, 0);
	}

	/**
	 * tworzy punkt (a,b), gdzie a,b są liczbami całkowitymi
	 *
	 * @param a
	 * @param b
	 */
	public Point(int a, int b) {
		x = a;
		y = b;
	}

	/**
	 * wyprowadza na konsolę informację o punkcie
	 */
	void show() {
		System.out.println(toString());
	}

	/**
	 * zwraca odległość (typu double) między danym punktem a punktem  p.
	 *
	 * @param p
	 * @return
	 */
	public double distance(Point p) {

		double x = p.getX() - getX();
		double y = p.getY() - getY();

		return sqrt(pow(x, 2) + pow(y, 2));
	}

	/**
	 * zwraca true jeśli odcinek łączący punkt p z danym punktem jest równoległy
	 * do jednej z osi układu współrzędnych, false w przeciwnym przypadku.
	 *
	 * @param p
	 * @return
	 */
	public boolean parallel(Point p) {
		return (p.getX() == getX()) || (p.getY() == getY()) ? true : false;
	}

	@Override
	public String toString() {
		return "Punkt: " + x + "," + y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
