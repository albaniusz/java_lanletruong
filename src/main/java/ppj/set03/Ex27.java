package ppj.set03;

import ppj.set03.ex27.Point;

/**
 * Zadanie 27
 *
 * Wzorując się na przykładzie, stworzyć własną klasę Point reprezentujacą
 * obiekty - punkty w układzie współrzędnych Oxy, z:
 *
 * a) konstruktorami:
 * Point(): tworzy punkt (0,0)
 * Point(int a): tworzy punkt (a,0), gdzie a jest liczbą całkowitą
 * Point(int a, int b): tworzy punkt (a,b), gdzie a,b są liczbami całkowitymi
 *
 * b)  metodami:
 * int getX(): zwraca współrzędną x typu int
 * int getY(): zwraca współrzędną y typu int
 * void setX(int a): ustawia wartość współrz. x typu int
 * void setY(int b): ustawia wartość współrz. y typu int
 * void show(): wyprowadza na konsolę informację o punkcie
 * double distance(Point p): zwraca odległość (typu double) między danym punktem
 * a punktem  p.
 * boolean parallel(Point p): zwraca true jeśli odcinek łączący punkt p z danym
 * punktem jest równoległy do jednej z osi układu współrzędnych, false
 * w przeciwnym przypadku.
 *
 * Napisać program, który tworzy obiekty klasy Point i testuje działania
 * WSZYSTKICH metod.  Dane wejściowe (współrzędne, promienie) podać
 * w inicjacji odpowiednich zmiennych w programie
 */
public class Ex27 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Point point1 = new Point();
		Point point2 = new Point(5);
		Point point3 = new Point(6, 8);

		System.out.println(point1);
		System.out.println(point2);
		System.out.println(point3);

		System.out.println("zmiana parametrów point1");
		point1.setX(5);
		point1.setY(5);
		System.out.println(point1);

		System.out.println("odległość pomiędzy punktami point1 a point2");
		System.out.println(point1.distance(point2));

		System.out.println(point1.parallel(point2));
	}
}
