package ppj.set03;

import ppj.set03.ex27.Point;
import ppj.set03.ex28.Circle;

/**
 * Zadanie 28
 *
 * Stworzyć własną klasę Circle (korzystając z własnej klasy Point z zadania 27)
 * reprezentujacą obiekty - koła w układzie wspólrzędnych Oxy, z:
 *
 * a) konstruktorami:
 *
 * Circle(): tworzy koło z środkiem (0,0) i promieniem 1
 * Circle(int r): tworzy koło z środkiem w punkcie (0,0), i promieniem całkowitym r
 * Circle(Point p, int r): tworzy koło z środkiem w punkcie p, i promieniem całkowitym r
 *
 * b) metodami:
 * Point getCenter(): zwraca środek koła typu Point
 * int getRadius(): zwraca promień koła typu int
 * void setCenter(Point c): ustawia środek koła typu Point
 * void setRadius(int r): ustawia promień koła typu int
 * void show(): wyprowadza na konsolę informację o kole
 * boolean inside(Point p): sprawdza, czy punkt p leży wewnątrz danego koła
 * boolean isBigger(Circle c): sprawdza, czy dane koło jest większe niż koło c
 * double area(): zwraca pole koła typu double.
 * boolean intersection(Circle p): zwraca true jeśli dane koło i koło p
 * się przecinają, false w przeciwnym przypadku.
 *
 * Napisać program, który tworzy obiekty klasy Point i testuje działania
 * WSZYSTKICH metod.  Dane wejściowe (współrzędne, promienie) podać
 * w inicjacji odpowiednich zmiennych w programie
 */
public class Ex28 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Circle circle1 = new Circle();
		Circle circle2 = new Circle(5);
		Circle circle3 = new Circle(new Point(), 7);

		System.out.println(circle1.getCenter());
		System.out.println(circle1.getRadius());

		circle1.setCenter(new Point(5, 5));
		circle1.setRadius(5);

		System.out.println(circle1.getCenter());
		System.out.println(circle1.getRadius());

		circle1.show();

		System.out.println(circle1.inside(new Point(-2, -2)));
		System.out.println(circle1.isBigger(circle2));

		System.out.println(circle1.area());
		System.out.println(circle1.intersection(circle3));
	}
}
