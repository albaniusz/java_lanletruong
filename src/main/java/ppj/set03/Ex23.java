package ppj.set03;

import java.util.Random;

/**
 * ZADANIE 23
 *
 * Napisać program, który stwierdza czy liczba wylosowana przez komputer
 * znajduje się w przedziale liczb, podanym przez użytkownika. Użytkownik podaje
 * dwa końce (całkowite) przedziału liczb [a, b] (z założeniem, że a < b oraz a,
 * b < 100). Komputer losuje liczbę całkowitą z przedziału [0,100). Jeśli
 * wylosowana liczba znajduje się poza przedziałem [a, b], program wyświetla
 * komunikat w postaci: Wylosowana liczba to ..., znajduje się ona poza
 * przedzialem [..., ...]. Komputer powtarza w/w czynność do momentu wylosowania
 * liczby całkowitej z przedziału [a, b], wtedy wyświetla informację
 * o wylosowanej liczbie oraz ilości nieudanych prób. Można skorzystać
 * z Math.random().
 */
public class Ex23 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int a = 43, b = 54, failedCounter = 0;

		Random generator = new Random();

		while (true) {
			int i = generator.nextInt(100);

			if (i < a || i > b) {
				System.out.println("Liczba " + i + " jest spoza przedziału ["
						+ a + ", " + b + "]");
				failedCounter++;
			} else {
				System.out.println("Liczba " + i + " jest z przedziału [" + a
						+ ", " + b + "]");
				break;
			}
		}
		System.out.println("Liczba nieudanych prób: " + failedCounter);
	}
}
