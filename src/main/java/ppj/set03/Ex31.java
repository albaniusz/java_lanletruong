package ppj.set03;

import ppj.set03.ex31.Banknot;
import ppj.set03.ex31.Bankomat;

/**
 * ZADANIE 31
 *
 * Napisać program symulujący operacje na prostych bankomatach. Z bankomatu
 * można wypłacić tylko banknoty o nominałach: 10 zł, 20 zł, 50 zł i 100 zł.
 * Bankomat, o ile jest to możliwe, wydaje żądaną kwotę pieniędzy klientowi,
 * korzystając z jak najmniejszej ilości banknotów. W przypadku braku możliwości
 * wypłacenia podanej kwoty, bankomat wyprowadza odpowiedni komunikat.
 * Użytkownik powinien mieć możliwość zmodyfikowania stanu bankomatu, dotyczy
 * to ilości pieniędzy dostępnych w bankomacie, w jakich nominałach oraz
 * dla każdego nominału w jakiej ilości.
 *
 * Uzupełnić klasę Bankomat w taki sposób, aby następujący program:
 *
 * public class Bankomat {
 *
 * // definicja typu wyliczeniowego: banknoty
 * enum Banknot {dziesiec, dwadziescia,  piedziesiac, sto};
 *
 *
 * // miejsce na pola, metody, konstruktory, ...
 * //  ...
 *
 *
 *
 * // funkcja testująca
 * public static void main(String[] args)
 * {
 * Bankomat b = new Bankomat();                    // tworzenie pustego bankomatu
 *
 * b.laduj(10, Banknot.sto);                               // ładowanie bankomatu banknotami podanego nominału o podanej ilości
 * b.laduj(20, Banknot.piedziesiac);
 * b.laduj(30, Banknot.dwadziescia);
 * b.laduj(40, Banknot.dziesiec);
 *
 * b.stan();                 // aktualny stan bankomatu
 *
 * b.wyplac(1010);    // o ile jest to możliwe, wypłacamy z bankomatu podaną kwotę za pomocą jak najmniejszej ilości banknotów
 * b.stan();
 *
 * b.laduj(1, Banknot.sto);
 * b.stan();
 *
 * b.wyplac(1030);
 * b.stan();
 *
 * b.wyplac(912);
 * b.stan();
 *
 * b.wyplac(50);
 * b.stan();
 *
 * b.wyplac(1011);
 * b.stan();
 * }
 * }
 *
 * wyprowadził na konsolę poniższe wyniki:
 * Stan bankomatu: 3000 zł (40 x 10zł, 30 x 20zł, 20 x 50zł, 10 x 100zł)
 *
 * Wypłata: 1010 zł -- 1 x 10zł  10 x 100zł
 * Stan bankomatu: 1990 zł (39 x 10zł, 30 x 20zł, 20 x 50zł, 0 x 100zł)
 *
 * Stan bankomatu: 2090 zł (39 x 10zł, 30 x 20zł, 20 x 50zł, 1 x 100zł)
 *
 * Wypłata: 1030 zł -- 1 x 10zł  1 x 20zł  18 x 50zł  1 x 100zł
 * Stan bankomatu: 1060 zł (38 x 10zł, 29 x 20zł, 2 x 50zł, 0 x 100zł)
 *
 * Wypłata: 912 zł = Brak możliwości wypłaty!
 * Stan bankomatu: 1060 zł (38 x 10zł, 29 x 20zł, 2 x 50zł, 0 x 100zł)
 *
 * Wypłata: 50 zł -- 1 x 50zł
 * Stan bankomatu: 1010 zł (38 x 10zł, 29 x 20zł, 1 x 50zł, 0 x 100zł)
 *
 * Wypłata: 1011 zł = Brak możliwości wypłaty!
 * Stan bankomatu: 1010 zł (38 x 10zł, 29 x 20zł, 1 x 50zł, 0 x 100zł)
 */
public class Ex31 {

	/**
	 * funkcja testująca
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		// tworzenie pustego bankomatu
		Bankomat b = new Bankomat();

		// ładowanie bankomatu banknotami podanego nominału o podanej ilości
		b.laduj(10, Banknot.sto);
		b.laduj(20, Banknot.piedziesiac);
		b.laduj(30, Banknot.dwadziescia);
		b.laduj(40, Banknot.dziesiec);

		// aktualny stan bankomatu
		b.stan();

		// o ile jest to możliwe, wypłacamy z bankomatu podaną kwotę za pomocą
		// jak najmniejszej ilości banknotów
		b.wyplac(1010);
		b.stan();

		b.laduj(1, Banknot.sto);
		b.stan();

		b.wyplac(1030);
		b.stan();

		b.wyplac(912);
		b.stan();

		b.wyplac(50);
		b.stan();

		b.wyplac(1011);
		b.stan();
	}
}
