package ppj.set03;

import ppj.set03.ex29.Counter;

/**
 * Zadanie 29 (2p)
 *
 * Zaprojektować i testować klasę Counter reprezentującą liczniki:
 * Pole: private int state - stan licznika
 * private static int op - rejestruje ilość operacji wyk. na obiektach klasy
 * Counter do chwili obecnej
 * Konstruktor: public Counter() - ustawia początkowy stan = 0
 * Metody: public int state() - zwraca aktualny stan licznika
 * public Counter inc() - zwiększa stan o 1 i zwraca  licznik
 * public Counter dec() - zmniejsza stan o 1 i zwraca licznik
 * public void reset() - zerować licznik
 * static int getOp() - zwraca liczbę operacji wyk. na obiektach klasy Counter
 * do chwili obecnej.
 */
public class Ex29 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Counter counter1 = new Counter();
		Counter counter2 = new Counter();
		Counter counter3 = new Counter();

		counter1.inc();
		counter1.inc();

		System.out.println("Stan licznika nr.1: " + counter1.state());

		counter1.inc();
		counter1.inc();

		System.out.println("Stan licznika nr.1: " + counter1.state());

		counter2.inc();
		counter2.inc();
		counter2.inc();

		System.out.println("Stan licznika nr.2: " + counter1.state());

		counter2.inc();
		counter2.inc();

		System.out.println("Stan licznika nr.2: " + counter1.state());

		counter2.reset();

		System.out.println("Stan licznika nr.2: " + counter1.state());

		counter3.inc();
		counter3.inc();
		counter3.dec();
		counter3.inc();
		System.out.println("Stan licznika nr.3: " + counter1.state());

		System.out.println("Liczba operacji na licznikach: " + Counter.getOp());
	}
}
