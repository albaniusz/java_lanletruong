package ppj.set03.ex31;

/**
 * Banknot
 *
 * definicja typu wyliczeniowego: banknoty
 */
public enum Banknot {

	dziesiec(10),
	dwadziescia(20),
	piedziesiac(50),
	sto(100);

	int value;

	/**
	 * @param value
	 */
	private Banknot(int value) {
		this.value = value;
	}
}
