package ppj.set03.ex30;

/**
 * Complex
 */
public class Complex {

	/**
	 * część rzeczywista i zesplona
	 */
	private double im, re;

	/**
	 * konstruuje liczbę zespoloną: 0 + i*0
	 */
	public Complex() {
		this(0.0, 0.0);
	}

	/**
	 * konstruuje liczbę zespoloną: re + i*im
	 *
	 * @param re
	 * @param im
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}

	/**
	 * konstruuje liczbę zespoloną: re + i*0
	 *
	 * @param re
	 */
	public Complex(double re) {
		this(re, 0.0);
	}

	/**
	 * konstruktor "kopiujący", tworzący identyczną kopię liczby zespolonej c
	 *
	 * @param c
	 */
	public Complex(Complex c) {
		this(c.getRe(), c.getIm());
	}

	/**
	 * zwraca re
	 *
	 * @return
	 */
	public double getRe() {
		return re;
	}

	/**
	 * zwraca im
	 *
	 * @return
	 */
	public double getIm() {
		return im;
	}

	/**
	 * zwraca tekstową reprezentację liczby zespolonej, w postaci: re + i*im
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return "" + (int) re + " + " + (int) im + "i";
	}

	/**
	 * zwraca nową liczbę zespoloną będącą sumą dwóch liczb zespolonych
	 *
	 * @param c
	 * @return
	 */
	public Complex add(Complex c) {
		return new Complex(getRe() + c.getRe(), getIm() + c.getIm());
	}

	/**
	 * zwraca nową liczbę zespoloną poprzez mnożenie przez liczbę rzeczywistą a
	 *
	 * @param a
	 * @return
	 */
	public Complex mult(double a) {
		return mult(new Complex(a));
	}

	/**
	 * zwraca nową liczbę zespoloną poprzez mnożenie przez liczbę zespoloną c
	 *
	 * @param c
	 * @return
	 */
	public Complex mult(Complex c) {

		double newRe = (getRe() * c.getRe()) + ((-1) * (getIm() * c.getIm()));
		double newIm = (getRe() * c.getIm()) + (getIm() * c.getRe());

		return new Complex(newRe, newIm);
	}
}
