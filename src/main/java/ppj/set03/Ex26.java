package ppj.set03;

/**
 * Zadanie 26
 *
 * Jakie wartości wypisze na konsolę następujący fragment programu. Wyjaśnić
 * działanie KAŻDEJ instrukcji bez pisania programu.
 *
 * String s1 = "a", s2  ="b", s3 = "ab";
 *
 * System.out.println(s1+s2);
 * System.out.println(s3);
 * System.out.println(s3==s1+s2);
 * System.out.println(s3.equals(s1+s2));
 * System.out.println(s3=="ab");
 * System.out.println(s3=="a"+"b");
 *
 * String s4 = new String("ab"), s5 = new String(s4);
 *
 * System.out.println(s3 == s4);
 * System.out.println(s4 == s5);
 * System.out.println(s4 == "ab");
 *
 * System.out.println(s4 == "a" + "b");
 * System.out.println(s4 == s5+"");
 * System.out.println(s4.equals(s5));
 * System.out.println(s4.equals(s5+""));
 *
 * s5 = s4 + "";
 * System.out.println(s4 == s5);
 *
 * String s6 = s5;
 * System.out.println(s5 == s6);
 * System.out.println(s5.equals(s6));
 *
 * s1="1";
 *
 * System.out.println(s1+2+3);
 * System.out.println((s1+2)+3);
 * System.out.println(s1+(2+3));
 * System.out.println(s1+(2/3));
 * System.out.println(s1+2/3);
 *
 * Sprawdzić swoją odpowiedź pisząc program, który pokazuje wyniki operacji.
 */
public class Ex26 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String s1 = "a", s2 = "b", s3 = "ab";

		System.out.println(s1 + s2);
		System.out.println(s3);
		System.out.println(s3 == s1 + s2);
		System.out.println(s3.equals(s1 + s2));
		System.out.println(s3 == "ab");
		System.out.println(s3 == "a" + "b");

		String s4 = new String("ab"), s5 = new String(s4);

		System.out.println(s3 == s4);
		System.out.println(s4 == s5);
		System.out.println(s4 == "ab");

		System.out.println(s4 == "a" + "b");
		System.out.println(s4 == s5 + "");
		System.out.println(s4.equals(s5));
		System.out.println(s4.equals(s5 + ""));

		s5 = s4 + "";
		System.out.println(s4 == s5);

		String s6 = s5;
		System.out.println(s5 == s6);
		System.out.println(s5.equals(s6));

		s1 = "1";

		System.out.println(s1 + 2 + 3);
		System.out.println((s1 + 2) + 3);
		System.out.println(s1 + (2 + 3));
		System.out.println(s1 + (2 / 3));
		System.out.println(s1 + 2 / 3);
	}
}
