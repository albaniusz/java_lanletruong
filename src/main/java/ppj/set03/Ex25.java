package ppj.set03;

/**
 * ZADANIE 25
 *
 * Napisać i testować (w main) własną funkcję: void kredyt(int kwotaKredytu,
 * int iloscLatKredytu, double roczneOproc), która realizuje symulację spłaty
 * kredytu:
 * Na podstawie podanych parametrów: kwoty kredytu, liczby lat kredytu oraz
 * rocznego oprocentowania, program wydrukuje wielkość raty dla każdego miesiąca
 * oraz sumę wszystkich rat po całym okresie zaciągania kredytu. Rata miesięczna
 * składa się z raty kapitałowej (miesięcznej) oraz z miesięcznego
 * oprocentowania kwoty kredytu pozostałej do spłacenia (tzw. raty malejące).
 */
public class Ex25 {

	/**
	 * @param kwotaKredytu
	 * @param iloscLatKredytu
	 * @param roczneOproc
	 */
	private static void kredyt(int kwotaKredytu, int iloscLatKredytu, double roczneOproc) {

		System.out.println("Kredyt na kwotę: " + kwotaKredytu + "; ilość lat: "
				+ iloscLatKredytu + ", oprocentowanie: " + roczneOproc + "%");

		double rataKapitalowa = kwotaKredytu / iloscLatKredytu;
		double sumaRat = 0;
		double oprocentowanieChwilowe;

		for (int i = 1; i <= iloscLatKredytu; i++) {
			kwotaKredytu -= rataKapitalowa;
			oprocentowanieChwilowe = (kwotaKredytu) * (roczneOproc / 100);

			System.out.println("Rata " + i + ": " + rataKapitalowa
					+ " oprocentowanie: " + oprocentowanieChwilowe
					+ " = " + (rataKapitalowa + oprocentowanieChwilowe));

			sumaRat += rataKapitalowa + oprocentowanieChwilowe;
		}

		System.out.println("Spłacona kwota: " + sumaRat);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		kredyt(250000, 30, 3.92);
	}
}
