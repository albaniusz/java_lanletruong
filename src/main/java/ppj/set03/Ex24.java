package ppj.set03;

/**
 * Zadanie24
 *
 * Napisać i testować (w main) własną funkcję: long silnia(int n), która oblicza
 * silnię n! podanej liczby n, wprowadzonej przez użytkownika.
 */
public class Ex24 {

	/**
	 * @param n
	 * @return
	 */
	private static long silnia(int n) {

		long results = 1;

		for (int i = 2; i <= n; i++) {
			results *= i;
		}

		return results;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		for (int i = 0; i <= 10; i++) {
			System.out.println(silnia(i));
		}
	}
}
