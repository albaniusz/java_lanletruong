package ppj.set03;

/**
 * Zadanie 22
 *
 * Napisać program, który dla dowolnej, podanej liczby n typu int oblicza
 * (za pomocą pętli) sumę według wzoru podanego po lewej stronie równości oraz,
 * w celu testowania poprawności obliczenia, porównuje obliczoną sumę z wynikiem
 * obliczenia według wzoru podanego po prawej stronie równości.
 *
 * a. (1p) 1*2+2*3+...+n*(n+1) = n*(n+1)*(n+2)/3
 * b. (1p) 1/2 + 1/4 + 1/8 + 1/16 + ... = 1
 * C. (2p) 1^2-2^2+3^2-4^2+...+(-1)^n-1 n^2= (-1)^n-1 *n*(n+1)/2
 */
public class Ex22 {

	/**
	 * @param number
	 * @param power
	 * @return
	 */
	private static int power(int number, int power) {

		int result = 1;

		for (int i = 1; i <= power; i++) {
			result *= number;
		}

		return result;
	}

	/**
	 * @param left
	 * @param right
	 */
	private static void printResult(int left, int right) {

		boolean status = left == right ? true : false;

		String message = (status ? "liczby są równe" : "liczby nie są równe")
				+ ": " + left + " = " + right;
		System.out.println(message);
	}

	/**
	 * @param n
	 */
	private static void methodA(int n) {

		int left = 0, right;

		for (int i = 1; i <= n; i++) {
			left += i * (i + 1);
		}

		right = (n * (n + 1) * (n + 2)) / 3;

		printResult(left, right);
	}

	/**
	 * @param n
	 */
	private static void methodB(int n) {

		int left = 0, right = 1;

		for (int i = 1; i <= n; i++) {
			left += 1 / i;
		}

		printResult(left, right);
	}

	/**
	 * @param n
	 */
	private static void methodC(int n) {

		int left = 0, right;
		boolean foo = true;

		for (int i = 1; i <= n; i++) {
			if (foo) {
				left += power(i, 2);
				foo = false;
			} else {
				left -= power(i, 2);
				foo = true;
			}
		}

		right = (power(-1, n - 1) * n * (n + 1)) / 2;

		printResult(left, right);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		methodA(5);
		methodB(5);
		methodC(5);
	}
}
